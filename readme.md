How to install this (https://bitbucket.org/Maddog415/base-jekyll-bootstrap) website, a Jekyll Bootstrap site with responsive images.

Npm, Grunt, Bower and Ruby must first be installed on your computer. They can be installed with the following commands:

Npm: ```sudo apt-get install npm```

Grunt: ```sudo npm install -g grunt-cli```

Bower: ```sudo npm install -g bower```

Ruby: ```sudo apt-get install ruby```

Once these have been installed, install Jekyll with the command:
```sudo gem install jekyll```

Clone the site from the Bitbucket link above by clicking “Clone” and copying the code provided. Create a directory for the site to be installed in, navigate to it and paste the line of code into the terminal.

Once this has been completed, install Grunt locally on your project with the command:

```npm install```

There may be an error with the node installation and you receive this error message every time you try to run a Grunt command:

```/usr/bin/env: node: No such file or directory```

This can be corrected by typing the following line of code:

```ln -s /usr/bin/nodejs /usr/bin/node```

Once grunt is installed you must install Bower locally on your project with the command:

```bower install```

This site uses the Jekyll image tag (https://github.com/robwierzbowski/jekyll-image-tag) plugin. This plugin requires both Minimagick (https://github.com/minimagick/minimagick) and [Imagemagick] (http://www.imagemagick.org/script/index.php) to work. You can install Minimagick with the command:

```sudo gem install mini_magick```

Follow the link above and install Imagemagick.

Once these have been installed download the Jekyll image tag ZIP file from the link above, extract it to your home directory (or any other directory of your choice), and copy the image_tag.rb file to your _plugins directory in the site.

This site uses redcarpet by default, it can be installed with the command:

```sudo gem install redcarpet```

Once you have completed all of the above instructions type the command “grunt” to deploy the site onto a local server.
