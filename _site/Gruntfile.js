module.exports = function(grunt) {

  // Don't forget to fix exclude: node_modules to _config.yml

  // load all grunt tasks
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  // Project configuration.
  grunt.initConfig({

    less: {
      development: {
        options: {
          //compress: true
          //paths: ['app/assets/_less']
        },
        files: {'app/assets/_less/main.min.css': [
            'app/assets/_less/style.less']
            }
        }
    },

    copy: {
      css : {
        files: {
          //'_site/css/main.min.css': 'app/assets/_less/main.min.css'
          'css/main.min.css': 'app/assets/_less/main.min.css'
        }
      }
    },

    shell: {
      jekyll: {
        command: 'rm -rf _site/*; jekyll build',
        stdout: true
      }
    },

    watch: {
      options: {
        livereload: true
      },
      less: {
        files: ['app/assets/_less/*.less'], // Expand this
        tasks: ['lessCopy']
      },
      jekyllSources: {
        files: [
            'css/*', // new line to trigger rebuild if css changes
            'assets/**',
            '_includes/**/*.html',
            '_includes/**/*.markdown',
            '_includes/**/*.md',
            '_layouts/*.html',
            '_posts/*.markdown',
            '_config.yml',
            'index.html',
            'img/*'
        ],
        tasks: ['shell:jekyll']
      }
    },

    connect: {
      server: {
        options: {
          base: '_site/',
          port: 9000
        }
      }
    },

    open: {
      server: {
        path: 'http://localhost:<%= connect.server.options.port %>/'
      }
    }
  });

  grunt.registerTask('lessCopy', ['less:development', 'copy:css']);

  grunt.registerTask('server', [
    'shell:jekyll', 'less:development', 'copy:css', //First build
    'connect:server',
    'open:server',
    'watch'
  ]);

  // Default task.
  grunt.registerTask('default', 'server');

};
